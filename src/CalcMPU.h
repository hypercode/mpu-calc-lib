//
// Created by dejan on 1. 11. 19.
//

#ifndef MPULIB_CALCMPU_H
#define MPULIB_CALCMPU_H

#include "src/Bolder_Flight_Systems_MPU9250/src/MPU9250.h"
#include "QuaternionFilter.h"

class CalcMPU {
public:

    float roll, pitch, yaw;
    float magnetic_declination = 3;


    void begin();


    bool available();


    void update();


};


#endif //MPULIB_CALCMPU_H
