//
// Created by dejan on 1. 11. 19.
//

#include "CalcMPU.h"
#include <Wire.h>


#define MPU9250_ADDRESS 0x68


MPU9250 IMU(Wire, MPU9250_ADDRESS);
QuaternionFilter qFilter;


float q[4] = {1.0f, 0.0f, 0.0f, 0.0f};
float a12, a22, a31, a32, a33;            // rotation matrix coefficients for Euler angles and gravity components
float lin_ax, lin_ay, lin_az;             // linear acceleration (acceleration with gravity component subtracted)




void CalcMPU::begin() {

    //Start wire connection
    Wire.begin();
    Wire.setClock(350000);

    delay(2000); //wait clock to change

    //start  connection to IMU
    int imu_status = IMU.begin();
    while (imu_status < 0) {
        Serial.println("IMU initialization unsuccessful");
        Serial.println("Check IMU wiring or try cycling power");
        Serial.print("Status: ");
        Serial.println(imu_status);
        yield(); //prevent ESP to sleep
    }

    // setting the accelerometer full scale range to +/-8G
    IMU.setAccelRange(MPU9250::ACCEL_RANGE_8G);
    // setting the gyroscope full scale range to +/-1000 deg/s
    IMU.setGyroRange(MPU9250::GYRO_RANGE_1000DPS);
    // setting DLPF bandwidth to 184 Hz
    IMU.setDlpfBandwidth(MPU9250::DLPF_BANDWIDTH_184HZ);
    // setting SRD to 1 for a 5000 Hz update rate
    IMU.setSrd(1);
}

bool CalcMPU::available() {
    return IMU.available();
}


void CalcMPU::update() {
    // Read data from sensor
    IMU.readSensor();


    //Update qfilter values.
    qFilter.update(-IMU.getAccelX_mss(),
                   IMU.getAccelY_mss(),
                   IMU.getAccelZ_mss(),
                   IMU.getGyroX_rads(),
                   -IMU.getGyroY_rads(),
                   -IMU.getGyroZ_rads(),
                   IMU.getMagY_uT(), -IMU.getMagX_uT(), IMU.getMagZ_uT(), q);



    //calculate roll pitch and yaw angles
    a12 = 2.0f * (q[1] * q[2] + q[0] * q[3]);
    a22 = q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3];
    a31 = 2.0f * (q[0] * q[1] + q[2] * q[3]);
    a32 = 2.0f * (q[1] * q[3] - q[0] * q[2]);
    a33 = q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3];
    pitch = -asinf(a32);
    roll = atan2f(a31, a33);
    yaw = atan2f(a12, a22);
    pitch *= 180.0f / PI;
    roll *= 180.0f / PI;
    yaw *= 180.0f / PI;
    yaw += magnetic_declination;


    //correct mis-rotated angles
    if (roll > 0.f) roll -= 180;
    else if (roll < 0.f) roll += 180;

    //if (yaw >= +180.f) yaw -= 360.f;
    //else if (yaw < -180.f) yaw += 360.f;


    //Calculate acclearations without gravity.
    lin_ax = IMU.getAccelX_mss() + a31;
    lin_ay = IMU.getAccelY_mss() + a32;
    lin_az = IMU.getAccelZ_mss() - a33;

}