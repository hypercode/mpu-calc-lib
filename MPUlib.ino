
#include "src/CalcMPU.h"

CalcMPU mpu;

void setup(){
    Serial.begin(500000);

    mpu.begin();

    mpu.magnetic_declination = 3;



}



unsigned long last_updated =  0;
void loop(){

    if(mpu.available()){
        mpu.update();

        Serial.print("r: ");
        Serial.print(mpu.roll);
        Serial.print(" p: ");
        Serial.print(mpu.pitch);
        Serial.print(" y: ");
        Serial.print(mpu.yaw);
        Serial.println(" uS: " + String( micros() - last_updated));
        last_updated = micros();
    }

}